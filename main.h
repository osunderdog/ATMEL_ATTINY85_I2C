
// Standard AVR includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>

//#include "./USI_TWI_Slave/USI_TWI_Slave.h"
#include "./USI_TWI_Slave/usiTwiSlave.h"

//LED block
//DDBR Data Direction Register designate pins as in/out.
#define LED_REG DDRB

//PORT Definition Data is read or written here
#define LED_PORT PORTB

//LED indicators
#define R_LED PB3
#define Y_LED PB4

