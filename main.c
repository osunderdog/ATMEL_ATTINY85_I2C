
#include "main.h" 

//not sure how to get and set data in slave..
  //   master write mode (slave read)
  //
  // i2cset on master
  // i2cset -y 1 0x1e 0x44 0x10
  //
  // slave receives byte
  // slave acks to the master.

// https://github.com/Arachnid/busdrivershield/blob/master/firmware/main.c

void onLEDPortB(uint8_t led_pin);
void offLEDPortB(uint8_t led_pin);

//i2cget called
uint8_t i2c_read(uint8_t reg) {
  onLEDPortB(Y_LED);
  _delay_ms(4);
  offLEDPortB(Y_LED);
  return 0x44;
}

//i2cset called
void i2c_write(uint8_t reg, uint8_t value) {
  onLEDPortB(R_LED);
  _delay_ms(4);
  offLEDPortB(R_LED);
}


// -------- Global Variables --------- //

// -------- Functions --------- //
void onLEDPortB(uint8_t led_pin) {
  LED_PORT |= (1 << led_pin);
}

void offLEDPortB(uint8_t led_pin) {
  LED_PORT &= ~(1 << led_pin);
}

int main(void) {
  //slave address is shifted up by one to make space for read/write bit.
  uint8_t TWI_slaveAddress = 0b00011000;

  
  uint8_t startup_signal = 10;
  
  // -------- Inits --------- //
  /* initializing yellow and green led for output */
  LED_REG |= (1 << R_LED) | (1 << Y_LED);

  //blink out a startup code...
  while(startup_signal > 0) {
      onLEDPortB(R_LED);
      _delay_ms(1);
      offLEDPortB(R_LED);
      _delay_ms(5);
      startup_signal--;
    }

  usiTwiSlaveInit(TWI_slaveAddress, i2c_read, i2c_write);
  
  //must enable interrupts in order for USI_TWI to work.
  sei();
  
  // ------ Event loop ------ //
  while (1) {
    
    
    
  }
  return (0);
}

